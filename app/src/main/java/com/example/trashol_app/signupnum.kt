package com.example.trashol_app

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_signupnum.*

class signupnum : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signupnum)
        masuk.setOnClickListener{
            val intent = Intent(this,login::class.java)
            startActivity(intent)
        }
    }
    fun signupgoogle(view: View) {
        val intent = Intent(this,signupemail::class.java)
        startActivity(intent)
    }

    fun lanjutkan(view: View) {
        val intent = Intent(this, verify::class.java)
        startActivity(intent)
    }
}