package com.example.trashol_app.fragments

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.trashol_app.R
import kotlinx.android.synthetic.main.dialog_forum.*
import kotlinx.android.synthetic.main.dialog_sharing.*
import kotlinx.android.synthetic.main.fragment_driver.*
import kotlinx.android.synthetic.main.fragment_sharing.*
import kotlinx.android.synthetic.main.fragment_sharing.back

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [SharingFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class SharingFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }

    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sharing, container, false)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment SharingFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                SharingFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val home = HomeFragment()
        share.setOnClickListener{
            val dialog = AlertDialog.Builder(activity!!)
            dialog.setView(View.inflate(activity!!,R.layout.dialog_sharing,null))
            val myAlertDialog = dialog.show()
            myAlertDialog.yes.setOnClickListener {
                Toast.makeText(activity,"Informasi berhasil dibagikan", Toast.LENGTH_SHORT).show()
                myAlertDialog.dismiss()
                val info = InformationFragment()
                fragmentManager?.beginTransaction()?.apply {
                    replace(R.id.fl_wrapper,info)
                    commit()
                }
            }
            myAlertDialog.no.setOnClickListener {
                myAlertDialog.dismiss()
            }
        }
        back.setOnClickListener {
            fragmentManager?.beginTransaction()?.apply {
                replace(R.id.fl_wrapper,home)
                        .commit()
            }
        }
    }


}