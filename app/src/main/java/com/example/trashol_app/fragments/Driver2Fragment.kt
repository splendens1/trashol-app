package com.example.trashol_app.fragments

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.trashol_app.R
import com.example.trashol_app.login
import kotlinx.android.synthetic.main.dialog_driver.view.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [Driver2Fragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class Driver2Fragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
        Handler(Looper.getMainLooper()).postDelayed({
            val myDialogView = View.inflate(activity!!,R.layout.dialog_driver, null)
            val dialog = AlertDialog.Builder(activity!!)
            dialog.setView(myDialogView)
            val myAlertDialog = dialog.show()
            val driver3 = Driver3Fragment()
            myDialogView.ok.setOnClickListener {
                fragmentManager?.beginTransaction()?.apply {
                    replace(R.id.fl_wrapper,driver3)
                    commit()
                }
                myAlertDialog.dismiss()
            }
        },1000)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_driver2, container, false)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment Driver2Fragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            Driver2Fragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}