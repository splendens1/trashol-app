package com.example.trashol_app

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.trashol_app.fragments.*
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.dialog_logout.view.*
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_sharing.*

class home : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        val homeFragment = HomeFragment()
        val profilFragment = ProfilFragment()
        val aboutfragment = AboutFragment()

        makeCurrentFragment(homeFragment)

        bottom_navigation.setOnNavigationItemSelectedListener {
            when (it.itemId){
                R.id.ic_home->makeCurrentFragment(homeFragment)
                R.id.ic_profil->makeCurrentFragment(profilFragment)
                R.id.ic_info->makeCurrentFragment(aboutfragment)
                R.id.ic_logout->getAlertDialog()
            }
            true
        }
    }

    private fun makeCurrentFragment(fragment: Fragment) =
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.fl_wrapper, fragment)
            commit()
        }

    private fun getAlertDialog(){
        val myDialogView = LayoutInflater.from(this).inflate(R.layout.dialog_logout, null)
        val dialog = AlertDialog.Builder(this)
                .setView(myDialogView)
                .setCancelable(false)
        val myAlertDialog = dialog.show()
        myDialogView.yeslogout.setOnClickListener{keluar()}
        myDialogView.nologout.setOnClickListener{myAlertDialog.dismiss()}
    }
    fun keluar() {
        val intent = Intent(this,login::class.java)
        startActivity(intent)
    }
}