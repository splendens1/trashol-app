package com.example.trashol_app

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast

class verify : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_verify)
    }

    fun verifikasi(view: View) {
        Toast.makeText(this,"Verifikasi Berhasil",Toast.LENGTH_SHORT).show()
        val intent = Intent(this, login::class.java)
        startActivity(intent)
    }
}